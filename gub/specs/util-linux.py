#
from gub import tools
from gub import target

class Util_linux__tools (tools.AutoBuild):
    # util-linux-2.22.2 could not compile for tools::util-linux.
    source = 'https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v2.16/util-linux-ng-2.16.tar.gz'
    dependencies = ['libtool', 'bash']
    patches = ['util-linux-2.16-sysmacro.patch']
    configure_flags = (tools.AutoBuild.configure_flags
                + ' --disable-tls'
                + ' --disable-makeinstall-chown'
                + ' SHELL=%(tools_prefix)s/bin/bash'
                + ''' CFLAGS='-DLINE_MAX=1024' ''')

class Util_linux (target.AutoBuild):
    # util-linux-2.23 requires O_CLOEXEC but our glibc does not have it.
    source = 'https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v2.22/util-linux-2.22.2.tar.xz'
    dependencies = [ 'tools::gettext', 'tools::xzutils', 'libtool' ]
    configure_flags = (target.AutoBuild.configure_flags
                + ' --without-ncurses'
                + ' --enable-libuuid' # enable only libuuid
                + ' --disable-libblkid'
                + ' --disable-libmount'
                + ' --disable-mount'
                + ' --disable-losetup'
                + ' --disable-fsck'
                + ' --disable-partx'
                + ' --disable-uuidd'
                + ' --disable-mountpoint'
                + ' --disable-fallocate'
                + ' --disable-unshare'
                + ' --disable-eject'
                + ' --disable-agetty'
                + ' --disable-cramfs'
                + ' --disable-wdctl'
                + ' --disable-switch_root'
                + ' --disable-pivot_root'
                + ' --disable-kill'
                + ' --disable-utmpdump'
                + ' --disable-rename'
                + ' --disable-chsh-only-listed'
                + ' --disable-login'
                + ' --disable-sulogin'
                + ' --disable-su'
                + ' --disable-schedutils'
                + ' --disable-wall'
                + ' --disable-pg-bell'
                + ' --disable-require-password'
                + ' --disable-use-tty-group'
                + ' --disable-makeinstall-chown'
                + ' --disable-makeinstall-setuid'
                )
    def install (self):
        target.AutoBuild.install (self)
        # Some unnecesary util-linux modules cannot be disabled.
        # So, delete those files.
        self.system ('''
rm -fr %(install_root)s/bin %(install_root)s/sbin
rm -fr %(install_prefix)s/bin %(install_prefix)s/sbin
rm -fr %(install_prefix)s/share
''', locals ())
